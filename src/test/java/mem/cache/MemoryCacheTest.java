package mem.cache;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class MemoryCacheTest {

    @Test
    public void getInstance() {

    }

    @Test
    public void loadTemporarily() throws IOException {
        MemoryCache cache = MemoryCache.getInstance();
        cache.put("abc","acropolis");
        System.out.println("Value of ABC is:"+cache.get("abc"));
        System.out.println("Value of DEF is:"+cache.get("def"));
    }

    @Test
    public void put() {
    }

    @Test
    public void put1() {
    }

    @Test
    public void get() {
    }

    @Test
    public void getLoadedObject() {
    }
}