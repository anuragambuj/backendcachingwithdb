package mem.cache;

import net.spy.memcached.MemcachedClient;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.io.IOException;
import java.net.InetSocketAddress;

public class MemoryCache implements PersistentCache {

    private static final InetSocketAddress MEMCACHE_HOST = new InetSocketAddress("127.0.0.1", 11211);
    private static MemcachedClient memcachedClient;
    public static final String LOADED_OBJECT_KEY = "LOADED_OBJECT_KEY";
    public static final int DEFAULT_MAX_LOAD_INTERVAL = 1000;
    private static String mutex = new String();

    static {
        try {
            memcachedClient = new MemcachedClient(MEMCACHE_HOST);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static MemoryCache memoryCache = null;

    private MemoryCache() throws IOException {

    }


    public static MemoryCache getInstance() throws IOException {
        MemoryCache tempMemoryCache = null;
        synchronized (mutex) {
            if (memoryCache == null) {
                memoryCache = new MemoryCache();
                tempMemoryCache = memoryCache;
            }else  if(tempMemoryCache==null && memoryCache!=null){
                tempMemoryCache = memoryCache;
            }
            return tempMemoryCache;
        }
    }

    public void loadTemporarily(Object object){
        if(memcachedClient!=null){
            memcachedClient.set(LOADED_OBJECT_KEY,DEFAULT_MAX_LOAD_INTERVAL,object);
        }else{
            throw new NullPointerException("Fatal Exception, Empty Memory Cache Object Encountered");
        }
    }

    /**
     * ]
     * @param key object must object have an overridden toString() non-compliance of this will cause a
     *            value to be inserted but lost causing unnecessary space hog and increase cost.
     *            Reviewers are asked to check the implementation.
     * @param value
     */
    public void put(Object key,Object value){
        if(memcachedClient!=null){
            memcachedClient.set(key.toString(),DEFAULT_MAX_LOAD_INTERVAL,value);
        }else{
            throw new NullPointerException("Fatal Exception, Empty Memory Cache Object Encountered");
        }
    }

    public void put(String key,int exp,Object value){
        if(memcachedClient!=null){
            if(exp<=0){
                exp=DEFAULT_MAX_LOAD_INTERVAL;
            }
            memcachedClient.set(key,exp,value);
        }else{
            throw new NullPointerException("Fatal Exception, Empty Memory Cache Object Encountered");
        }
    }

    @Override
    public void put(String key, Object value) {
        if(memcachedClient!=null){
            memcachedClient.set(key,DEFAULT_MAX_LOAD_INTERVAL,value);
        }else{
            throw new NullPointerException("Fatal Exception, Empty Memory Cache Object Encountered");
        }
    }

    public Object get(@NonNull String key){
        if(memcachedClient!=null) {
            return memcachedClient.get(key);
        }else{
            throw new NullPointerException("Fatal Exception, Empty Memory Cache Object Encountered");
        }
    }

    public Object getLoadedObject(){
        if(memcachedClient!=null && memcachedClient.get(LOADED_OBJECT_KEY)!=null){
            return memcachedClient.get(LOADED_OBJECT_KEY);
        }else{
            throw new NullPointerException("Fatal Exception, Null or Empty Memory Cache Object Encountered");
        }
    }
}
