package mem.cache;

public interface PersistentCache {
    public void put(String key,Object value);
    public Object get(String key);
}
